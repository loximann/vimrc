syntax on
filetype plugin on
filetype indent on

"" EnhancedCommentify bindings
let g:EnhCommentifyUseAltKeys="yes"
"let g:EnhCommentifyUserBindngs="yes"
"map c <plug>Comment
"map d <plug>DeComment
"map z <plug>Traditionalj
"map x <plug>Traditional

""""" USER INTERFACE TWEAKS
"" Show selection size in visual mode
set showcmd
"" Case sensitive only is capitals are present
set ignorecase
set smartcase
"" GUI options, such as have toolbar, scrollbars, etc
set guioptions=aegirLmTd
"" Visual bell
set vb
"" Right click modes cursor position and shows popup menu
set mousemodel=popup_setpos

set expandtab

""" Set <Leader> to ;
let maplocalleader=","

"""" COLORS FOR GVIM
set background=dark
colorscheme jellybeans
" Color tweaks

""" Size, lines, etc
""set textwidth=80
""set winwidth=80
""set columns=80
set scrolloff=15

" Highlight search
set hlsearch
" Pressing enter clears the search highlighting
nnoremap <CR> :noh<CR>
" Incremental search
set incsearch

" is.vim mappings
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)

" Highlight unwanted space
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()
" Show trailing whitepace and spaces before a tab:
:autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\t/

"" Spell check
let spell_executable = "aspell"
hi SpellErrors  guibg=Red guifg=Black cterm=underline gui=underline term=reverse

" Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
set viminfo='100,\"100,:20,%,n~/.viminfo
"
" Jump to last remembered position
autocmd BufReadPost *
\ if line("'\"") > 1 && line("'\"") <= line("$") |
\   exe "normal! g`\"" |
\ endif

""Syntastic syntax checker
let g:syntastic_mode_map      = { 'mode': 'active' }
let g:syntastic_check_on_open = 1

""local_vimrc   .vimrc files present in the local directory will be loaded.
let g:local_vimrc = '.lvimrc'

"Autocompleting filenames:
set wildmode=longest,full
set wildmenu

set shiftwidth=4
set tabstop=4

" ### AUTOCOMPLETIONS (UltiSnips, YouCompleteMe, SuperTab) ###
" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:ycm_always_populate_location_list = 1

"let g:ycm_filetype_whitelist = {'cpp' : 1, 'python' : 1}
let g:ycm_filetype_blacklist = {'*' : 1}

map <F10> :lprev<cr>
map <F11> :ll<cr>
map <F12> :lnext<cr>

let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

"  UltiSnips
"" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
""let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsExpandTrigger="<m-space>"
"let g:UltiSnipsJumpForwardTrigger="<F3>"
"let g:UltiSnipsJumpBackwardTrigger="<F2>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

"Directory for swap files
set directory=~/.vim/swap

"Nicer colors in console
set t_CO=256

" FSwitch (switch between header and implementation files)
nmap <silent> <LocalLeader>o :FSSplitRight<cr>
set switchbuf=usetab

"# Global mappings
map <F4> :make!<cr>
map <F5> :YcmCompleter FixIt<cr>
"## Go to definition using tags instead of vim's gd heuristics
map gd 
